#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>
#include <NeoPixelBus.h>
#include <NeoPixelAnimator.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>


#ifndef STASSID
#define STASSID "SSID_DU_WIFI"
#define STAPSK  "CLE_WIFI"
#endif

const char* ssid = STASSID;
const char* password = STAPSK;

ESP8266WebServer server(80);


//Variables
const uint16_t PixelCount = 1; 
const uint8_t PixelPin = 2;  // make sure to set this to the correct pin, ignored for Esp8266
const uint8_t AnimationChannels = 1; // we only need one as all the pixels are animated at once

char host[] = "ironman";


NeoPixelBus<NeoGrbFeature, Neo800KbpsMethod> strip(PixelCount, PixelPin);
RgbColor red(255, 0, 0);
RgbColor green(0, 255, 0);
RgbColor blue(0, 0, 255);
RgbColor white(255);
RgbColor black(0);

int state = 0; // 0=nothing, 1=finished, 2=failed, 3=error
uint32_t millisNow, lastMillis, lastHeartbeat = 0;

int blinkDuration = 60 * 1000; // durée du clignottement par défaut

int durationFast = 400;         // 400ms
int durationShort = 1 * 1000;   // 1 seconde
int durationMedium = 30 * 1000; // 30 secondes
int durationLong = 60 * 1000;   // 1 minute

int heartbeat = 10*60*1000; // heartbeat toutes les 10 minutes
int blinkPeriodSlow = 1000; // periode de clignottement (1 seconde)
int blinkPeriodMedium = 500; // periode de clignottement (500 ms)
int blinkPeriodFast = 100; // periode de clignottement (100 ms)

NeoPixelAnimator animations(AnimationChannels); // NeoPixel animation management object
boolean fadeToColor = true;  // general purpose variable used to store effect state
struct MyAnimationState
{
  RgbColor StartingColor;
  RgbColor EndingColor;
};
MyAnimationState animationState[AnimationChannels];

void SetRandomSeed()
{
  uint32_t seed;

  // random works best with a seed that can use 31 bits
  // analogRead on a unconnected pin tends toward less than four bits
  seed = analogRead(0);
  delay(1);

  for (int shifts = 3; shifts < 31; shifts += 3)
  {
    seed ^= analogRead(0) << shifts;
    delay(1);
  }

  randomSeed(seed);
}

// simple blend function
void BlendAnimUpdate(const AnimationParam& param)
{
  // this gets called for each animation on every time step
  // progress will start at 0.0 and end at 1.0
  // we use the blend function on the RgbColor to mix
  // color based on the progress given to us in the animation
  RgbColor updatedColor = RgbColor::LinearBlend(
                            animationState[param.index].StartingColor,
                            animationState[param.index].EndingColor,
                            param.progress);

  // apply the color to the strip
  for (uint16_t pixel = 0; pixel < PixelCount; pixel++)
  {
    strip.SetPixelColor(pixel, updatedColor);
  }

  /*
      if (param.progress == 1.0 && animationState[param.index].EndingColor == black) {
        colorIndex == sizeof(rainbow)-1 ? colorIndex = 0 : colorIndex++;
      }
  */

}

void FadeInFadeOutRinseRepeat(float luminance, RgbColor target, int period )
{
  if (fadeToColor)
  {
    // Fade upto a random color
    // we use HslColor object as it allows us to easily pick a hue
    // with the same saturation and luminance so the colors picked
    // will have similiar overall brightness



    animationState[0].StartingColor = strip.GetPixelColor(0);
    animationState[0].EndingColor = target;

    animations.StartAnimation(0, period, BlendAnimUpdate);
  }
  else
  {
    // fade to black

    animationState[0].StartingColor = strip.GetPixelColor(0);
    animationState[0].EndingColor = RgbColor(0);
    animations.StartAnimation(0, period, BlendAnimUpdate);
  }

  // toggle to the next effect state
  fadeToColor = !fadeToColor;
}

void handleRoot() {
  server.send(200, "text/plain", "");
  state = 0;
  blinkLed();
}

void handleNotFound() {
  server.send(404, "text/plain", "");
}

void setupOTA() {

  // Port defaults to 8266
  // ArduinoOTA.setPort(8266);

  // Hostname defaults to esp8266-[ChipID]
  ArduinoOTA.setHostname(host);

  // No authentication by default
  // ArduinoOTA.setPassword("PASSWORD");

  // Password can be set with it's md5 value as well
  // MD5(admin) = 21232f297a57a5a743894a0e4a801fc3
  // ArduinoOTA.setPasswordHash("21232f297a57a5a743894a0e4a801fc3");

  ArduinoOTA.onStart([]() {
    String type;
    if (ArduinoOTA.getCommand() == U_FLASH) {
      type = "sketch";
    } else { // U_FS
      type = "filesystem";
    }

    // NOTE: if updating FS this would be the place to unmount FS using FS.end()
    Serial.println("Start updating " + type);
  });

  ArduinoOTA.onEnd([]() {
    Serial.println("\nEnd");
  });

  ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
    Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
  });

  ArduinoOTA.onError([](ota_error_t error) {
    Serial.printf("Error[%u]: ", error);
    if (error == OTA_AUTH_ERROR) {
      Serial.println("Auth Failed");
    } else if (error == OTA_BEGIN_ERROR) {
      Serial.println("Begin Failed");
    } else if (error == OTA_CONNECT_ERROR) {
      Serial.println("Connect Failed");
    } else if (error == OTA_RECEIVE_ERROR) {
      Serial.println("Receive Failed");
    } else if (error == OTA_END_ERROR) {
      Serial.println("End Failed");
    }
  });

  ArduinoOTA.begin();
  Serial.println("Ready");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
}

void setupHTTP() {
  server.on("/", handleRoot);
  server.on("/favicon.ico", handleNotFound);

  server.on("/white", []() {
    server.send(200, "text/plain", "WHITE");
    state = 100;
    millisNow = millis();
    blinkLed();
  });

  server.on("/red", []() {
    server.send(200, "text/plain", "RED");
    state = 101;
    millisNow = millis();
    blinkLed();
  });

  server.on("/green", []() {
    server.send(200, "text/plain", "GREEN");
    state = 102;
    millisNow = millis();
    blinkLed();
  });

  server.on("/blue", []() {
    server.send(200, "text/plain", "BLUE");
    state = 103;
    millisNow = millis();
    blinkLed();
  });

  server.on("/heartbeat", []() {
    server.send(200, "text/plain", "HEARTBEAT");
    state = 200;
    millisNow = millis();
    blinkLed();
  });

  server.onNotFound(handleNotFound);

  server.begin();
  Serial.println("HTTP server started");
}

void setup(void) {
  Serial.begin(115200);
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  Serial.println("");
  lastHeartbeat = millis();

  // Wait for connection
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.print("Connected to ");
  Serial.println(ssid);
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());

  if (MDNS.begin(host)) {
    Serial.println("MDNS responder started");
  }
  strip.Begin();

  setupOTA();
  setupHTTP();

  state = 5;
  millisNow = millis();
  blinkLed();
}

void loop(void) {
  // heartbeat
  if ((millis() >= lastHeartbeat + heartbeat)) {
    lastHeartbeat = millis();
    state = 200;
    millisNow = millis();
    blinkLed();
  }

  if (WiFi.status() == WL_CONNECTED) {
    server.handleClient();
    ArduinoOTA.handle();
    MDNS.update();
    if (animations.IsAnimating())
    {
      if (millis() >= millisNow + blinkDuration ) { // animation over ? paint it black
        state = 0;
        blinkLed();
      }
      // Else, just go ahead
      animations.UpdateAnimations();
      strip.Show();
    } else {
      if (millis() <= millisNow + blinkDuration ) {
        blinkLed();
      }
    }
  } else {
    Serial.println("Connection lost... reconnect");
    WiFi.disconnect();
    WiFi.begin(ssid, password);
    state = 6;
    blinkLed();
    while (WiFi.status() != WL_CONNECTED) {
      millisNow = millis();
      animations.UpdateAnimations();
      strip.Show();
    }
    state = 0;
    millisNow = 0;
  }

}

void blinkLed() {

  switch (state) {
    case 0: if (strip.GetPixelColor(0) != black) {
        strip.SetPixelColor(0, black);
        strip.Show();
        if (animations.IsAnimating()) {
          animations.StopAnimation(0);
        }
      }
      break;

    case 5: FadeInFadeOutRinseRepeat(1.0f, white,   blinkPeriodFast); blinkDuration = durationShort; break;  // Boot

    case 100: FadeInFadeOutRinseRepeat(1.0f, white, blinkPeriodMedium); blinkDuration = durationMedium;  break;  
    case 101: FadeInFadeOutRinseRepeat(1.0f, red,   blinkPeriodMedium); blinkDuration = durationMedium;  break;
    case 102: FadeInFadeOutRinseRepeat(1.0f, green, blinkPeriodMedium); blinkDuration = durationMedium;  break;
    case 103: FadeInFadeOutRinseRepeat(1.0f, blue, blinkPeriodMedium); blinkDuration = durationMedium;  break;

    case 200: FadeInFadeOutRinseRepeat(1.0f, red,   blinkPeriodFast); blinkDuration = durationFast; break;
    default: break;
  }
}
